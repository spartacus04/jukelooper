package com.ranull.jukelooper.integrations;

import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

interface Integration {
    boolean cannotUse();

    boolean play(ItemStack itemStack, Location location);

    void stop(ItemStack itemStack, Location location);

    long getDuration(ItemStack itemStack);
}
