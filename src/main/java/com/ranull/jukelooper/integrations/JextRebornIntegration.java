package com.ranull.jukelooper.integrations;

import me.spartacus04.jext.State;
import me.spartacus04.jext.config.fields.FieldJukeboxBehaviour;
import me.spartacus04.jext.discs.Disc;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

public class JextRebornIntegration implements Integration {
    private final Plugin plugin;

    public JextRebornIntegration(Plugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean cannotUse() {
        Plugin jextReborn = plugin.getServer().getPluginManager().getPlugin("JukeboxExtendedReborn");
        if(jextReborn == null || !jextReborn.isEnabled())
            return true;

        try {
            Class.forName("me.spartacus04.jext.discs.Disc");
            return State.INSTANCE.getCONFIG().getJUKEBOX_BEHAVIOUR() != FieldJukeboxBehaviour.VANILLA;
        } catch (ClassNotFoundException ignored) { }

        return true;
    }

    @Override
    public boolean play(ItemStack itemStack, Location location) {
        if(cannotUse()) return false;

        Disc disc = Disc.Companion.fromItemstack(itemStack);

        if(disc == null) return false;

        // Java doesn't support default arguments, so we have to pass them manually
        disc.play(location, 4f, 1f);

        return true;
    }

    @Override
    public long getDuration(ItemStack itemStack) {
        if(cannotUse()) return -1;

        Disc disc = Disc.Companion.fromItemstack(itemStack);
        if(disc == null) return -1;

        if(disc.getDuration() != -1) return disc.getDuration() * 1000L;

        // fallback method, get duration like JukeboxExtended
        String namespace = disc.getNamespace();

        if (plugin.getConfig().isSet("settings/namespace/" + namespace)) {
            return plugin.getConfig().getLong("settings/namespace/" + namespace) * 1000;
        }

        return -1;
    }

    @Override
    public void stop(ItemStack itemStack, Location location) {
        if(cannotUse()) return;

        Disc disc = Disc.Companion.fromItemstack(itemStack);
        if(disc == null) return;

        Disc.Companion.stop(location, disc.getNamespace());
    }
}
