package com.ranull.jukelooper.integrations;

import me.tajam.jext.DiscContainer;
import me.tajam.jext.DiscPlayer;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

public class JukeboxExtendedIntegration implements Integration {
    private final Plugin plugin;

    public JukeboxExtendedIntegration(Plugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean cannotUse() {
        Plugin jukeboxExtended = plugin.getServer().getPluginManager().getPlugin("JukeboxExtended");
        if(jukeboxExtended == null || !jukeboxExtended.isEnabled())
            return true;

        try {
            Class.forName("me.tajam.jext.DiscContainer");
            return false;
        } catch (ClassNotFoundException ignored) { }

        return true;
    }

    @Override
    public boolean play(ItemStack itemStack, Location location) {
        if(cannotUse()) return false;

        try {
            DiscContainer container = new DiscContainer(itemStack);

            new DiscPlayer(container).play(location);
        } catch (IllegalStateException ignored) {
            return false;
        }

        return true;
    }

    @Override
    public long getDuration(ItemStack itemStack) {
        if (cannotUse()) return -1;

        try {
            String namespace = new DiscContainer(itemStack).getNamespace();

            if (plugin.getConfig().isSet("settings/namespace/" + namespace)) {
                return plugin.getConfig().getLong("settings/namespace/" + namespace) * 1000;
            }
        } catch (IllegalStateException ignored) { }

        return -1;
    }

    @Override
    public void stop(ItemStack itemStack, Location location) {
        if(cannotUse()) return;

        try {
            new DiscPlayer(new DiscContainer(itemStack)).stop(location);
        } catch (IllegalStateException ignored) {
        }
    }
}
