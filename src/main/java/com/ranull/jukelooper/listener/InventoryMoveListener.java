package com.ranull.jukelooper.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.inventory.InventoryType;

public class InventoryMoveListener implements Listener {

    // Disables the new jukebox item transfer mechanic in 1.19.4
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onInventoryMoveItem(InventoryMoveItemEvent event) {
        if(
            event.getDestination().getType() == InventoryType.JUKEBOX ||
            event.getSource().getType() == InventoryType.JUKEBOX) {
            event.setCancelled(true);
        }
    }
}
