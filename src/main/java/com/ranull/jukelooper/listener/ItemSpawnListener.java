package com.ranull.jukelooper.listener;

import com.ranull.jukelooper.JukeLooper;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Iterator;
import java.util.Map;

public class ItemSpawnListener implements Listener {
    private final JukeLooper plugin;

    public ItemSpawnListener(JukeLooper plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onItemSpawn(ItemSpawnEvent event) {
        ItemStack itemStack = event.getEntity().getItemStack();
        Iterator<Map.Entry<Location, Material>> iterator = plugin.getLooperManager().getEjectMap().entrySet().iterator();

        while (iterator.hasNext()) {
            Map.Entry<Location, Material> entry = iterator.next();

            if (entry.getValue() == itemStack.getType() && entry.getKey().distance(event.getLocation()) <= 1) {
                event.setCancelled(true);
                iterator.remove();
            }
        }
    }
}
