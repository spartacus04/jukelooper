package com.ranull.jukelooper.manager;

import com.ranull.jukelooper.JukeLooper;
import com.ranull.jukelooper.integrations.JextRebornIntegration;
import com.ranull.jukelooper.integrations.JukeboxExtendedIntegration;
import com.ranull.jukelooper.looper.Looper;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public class LooperManager {
    private final JukeLooper plugin;
    private final Map<Location, Looper> looperMap;
    private final Map<Location, Material> ejectMap;
    private final JextRebornIntegration jextRebornIntegration;
    private final JukeboxExtendedIntegration jukeboxExtendedIntegration;

    public LooperManager(JukeLooper plugin) {
        this.plugin = plugin;
        this.looperMap = new HashMap<>();
        this.ejectMap = new HashMap<>();

        this.jextRebornIntegration = new JextRebornIntegration(plugin);
        this.jukeboxExtendedIntegration = new JukeboxExtendedIntegration(plugin);

        startSecondTimer();
    }

    private void startSecondTimer() {
        new BukkitRunnable() {
            @Override
            public void run() {
                for (Map.Entry<Location, Looper> entry : looperMap.entrySet()) {
                    Looper looper = entry.getValue();

                    if (looper.getDuration() > 0
                            && (looper.getDuration() <= System.currentTimeMillis() - looper.getStartTime())) {
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                nextDisc(looper);
                            }
                        }.runTask(plugin);
                    }
                }
            }
        }.runTaskTimerAsynchronously(plugin, 0L, 5L);
    }

    public boolean hasLooper(Location location) {
        return looperMap.containsKey(location);
    }

    public Looper createLooper(Jukebox jukebox) {
        Looper looper = new Looper(jukebox.getLocation());

        looperMap.put(jukebox.getLocation(), looper);

        return looper;
    }

    public void removeLooper(Looper looper) {
        looperMap.remove(looper.getLocation());
    }

    public void disable() {
        for (Looper looper : looperMap.values()) {
            Jukebox jukebox = looper.getJukebox();

            if (jukebox != null) {
                try {
                    jukebox.stopPlaying();
                    jukebox.update(true, true);
                } catch (NoSuchMethodError ignored) {
                }
            }
        }
    }

    public void updateLooper(Looper looper) {
        updateLooper(looper, looper.getJukebox().getRecord());
    }

    public void updateLooper(Looper looper, ItemStack itemStack) {
        long duration = getDuration(looper);

        if (duration > 0) {
            looper.setStartTime(System.currentTimeMillis());
            looper.setDuration(duration);
            playDisc(looper.getJukebox(), itemStack);
        }
    }

    public void playDisc(Jukebox jukebox, ItemStack itemStack) {

        if(jukeboxExtendedIntegration.play(itemStack, jukebox.getLocation())) return;

        if(jextRebornIntegration.play(itemStack, jukebox.getLocation())) return;

        // Minecraft
        jukebox.setPlaying(itemStack.getType());
    }

    public long getDuration(Looper looper) {
        ItemStack itemStack = looper.getJukebox().getRecord();

        if(jukeboxExtendedIntegration.getDuration(itemStack) != -1) {
            return jukeboxExtendedIntegration.getDuration(itemStack);
        }

        if(jextRebornIntegration.getDuration(itemStack) != -1) {
            return jextRebornIntegration.getDuration(itemStack);
        }

        // Model Data
        if (itemStack.getItemMeta() != null && itemStack.getItemMeta().hasCustomModelData()
                && plugin.getConfig().isSet("settings/model-data/" + itemStack.getItemMeta().getCustomModelData())) {
            return plugin.getConfig().getLong("settings/model-data/"
                    + itemStack.getItemMeta().getCustomModelData()) * 1000;
        }

        // Minecraft
        if (plugin.getConfig().isSet("settings/material/" + itemStack.getType())) {
            return plugin.getConfig().getLong("settings/material/" + itemStack.getType()) * 1000;
        }

        return -1;
    }

    public Looper getLooper(Location location) {
        return looperMap.get(location);
    }

    public void nextDisc(Looper looper) {
        Jukebox jukebox = looper.getJukebox();
        Block storage = findStorage(looper.getLocation());

        if (jukebox != null && storage != null) {
            if (jukebox.getRecord().getType().isRecord()) {
                Block blockDown = looper.getLocation().getBlock().getRelative(BlockFace.DOWN);

                jukeboxExtendedIntegration.stop(jukebox.getRecord(), jukebox.getLocation());

                jextRebornIntegration.stop(jukebox.getRecord(), jukebox.getLocation());

                // Storage
                if (blockDown.getState() instanceof Hopper) {
                    Inventory inventory = ((Hopper) blockDown.getState()).getInventory();

                    if (inventory.firstEmpty() != -1) {
                        inventory.addItem(jukebox.getRecord());
                        ejectClearJukebox(jukebox);
                    } else if (putItemInStorage(jukebox.getRecord(), storage.getLocation())) {
                        ejectClearJukebox(jukebox);
                    }
                } else if (putItemInStorage(jukebox.getRecord(), storage.getLocation())) {
                    ejectClearJukebox(jukebox);
                }
            }

            ItemStack recordFromStorage = takeItemFromStorage(storage.getLocation());

            if (recordFromStorage != null) {
                jukebox.setRecord(recordFromStorage);
                jukebox.update(true, true);
                updateLooper(looper);
            } else if (jukebox.getRecord().getType().isRecord()) {
                updateLooper(looper);
            } else {
                removeLooper(looper);
            }
        }
    }

    public void ejectClearJukebox(Jukebox jukebox) {
        ejectMap.put(jukebox.getLocation().clone().add(0.5, 0.5, 0.5), jukebox.getRecord().getType());
        jukebox.setRecord(new ItemStack(Material.AIR));
        jukebox.setPlaying(Material.AIR);
        jukebox.getBlock().setType(Material.JUKEBOX);
        jukebox.eject();
        jukebox.update(true, true);
    }

    public Map<Location, Material> getEjectMap() {
        return ejectMap;
    }

    public boolean putItemInStorage(ItemStack itemStack, Location location) {
        Block block = location.getBlock();

        if (block.getState() instanceof Hopper) {
            ((Hopper) block.getState()).getInventory().addItem(itemStack);

            return true;
        } else if (block.getState() instanceof Chest) {
            ((Chest) block.getState()).getBlockInventory().addItem(itemStack);

            return true;
        }

        return false;
    }

    private ItemStack takeItemFromStorage(Location storage) {
        List<ItemStack> recordList = new ArrayList<>();

        if (storage.getBlock().getState() instanceof Chest) {
            Chest chest = (Chest) storage.getBlock().getState();

            for (ItemStack itemStack : chest.getBlockInventory().getContents()) {
                if (itemStack != null && itemStack.getType().isRecord()) {
                    recordList.add(itemStack);
                }
            }

            if (!recordList.isEmpty()) {
                Collections.shuffle(recordList);
                chest.getBlockInventory().removeItem(recordList.get(0));
            }
        } else if (storage.getBlock().getState() instanceof Hopper) {
            Hopper hopper = (Hopper) storage.getBlock().getState();

            for (ItemStack itemStack : hopper.getInventory().getContents()) {
                if (itemStack != null && itemStack.getType().isRecord()) {
                    recordList.add(itemStack);
                }
            }

            if (!recordList.isEmpty()) {
                Collections.shuffle(recordList);
                hopper.getInventory().removeItem(recordList.get(0));
            }
        }

        return !recordList.isEmpty() ? recordList.get(0) : null;
    }

    public Block findStorage(Location location) {
        BlockState blockState = location.getBlock().getRelative(BlockFace.UP).getState();

        return blockState instanceof Chest || blockState instanceof Hopper ? blockState.getBlock() : null;
    }
}
